﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Floberman.Model
{
   public class Contact
    {
        public int code { set; get; }
        public string name { set; get; }
        public string client { set; get; }
        public string address { set; get; }
        public string phone { set; get; }
    }
}
