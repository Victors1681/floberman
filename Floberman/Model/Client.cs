﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Floberman.Model
{
    public class Client
    {
        public string code { set; get; }
        public string name { set; get; }
        public string address { set; get; }
        public string phone { set; get; }
        public float discount { set; get; }
        public int contactQty { set; get; }
    }
}
