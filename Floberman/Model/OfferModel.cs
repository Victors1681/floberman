﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Floberman.Model
{
    public class OfferModel
    {
        public string offerId { get; set; }
        public string clientId { get; set; }
        public string contactId { get; set; }
        public string productId { get; set; }
        public float discount { get; set; }
        public DateTime modified { get; set; }
        public string userId { get; set; }

    }
}
