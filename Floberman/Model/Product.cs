﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Floberman.Model
{
   public class Product
    {
        public string productId { get; set; }
        public string name { get; set; }
        public double stock { get; set; }
        public double price1 { get; set; }
        public double price2 { get; set; }
        public double price3 { get; set; }
        public double price4 { get; set; }
        public double price5 { get; set; }

        public double price1Itbis { get; set; }
        public double price2Itbis { get; set; }
        public double price3Itbis { get; set; }
        public double price4Itbis { get; set; }
        public double price5Itbis { get; set; }

    }
}
