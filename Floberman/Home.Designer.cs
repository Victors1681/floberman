﻿namespace Floberman
{
    partial class Home
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.clientGroup = new System.Windows.Forms.GroupBox();
            this.contactCodeLabel = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.clientPhoneLabel = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.clientCodeLabel = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.contactQtyLabel = new System.Windows.Forms.Label();
            this.findContactBtn = new System.Windows.Forms.Button();
            this.findClientBtn = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.contactTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.clientTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.clientDiscountLabel = new System.Windows.Forms.Label();
            this.discountDownLabel = new System.Windows.Forms.Label();
            this.addOffertBtn = new System.Windows.Forms.Button();
            this.offerGridView = new System.Windows.Forms.DataGridView();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.label5 = new System.Windows.Forms.Label();
            this.addOfferContainer = new System.Windows.Forms.GroupBox();
            this.discountAmountLabel = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.discountPriceLabel = new System.Windows.Forms.Label();
            this.priceLabel = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.productNameLabel = new System.Windows.Forms.Label();
            this.findProductBtn = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.discountTextBox = new System.Windows.Forms.TextBox();
            this.productTextBox = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.priceTextBox = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.clientGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.offerGridView)).BeginInit();
            this.addOfferContainer.SuspendLayout();
            this.SuspendLayout();
            // 
            // clientGroup
            // 
            this.clientGroup.BackColor = System.Drawing.Color.White;
            this.clientGroup.Controls.Add(this.contactCodeLabel);
            this.clientGroup.Controls.Add(this.label13);
            this.clientGroup.Controls.Add(this.clientPhoneLabel);
            this.clientGroup.Controls.Add(this.label8);
            this.clientGroup.Controls.Add(this.clientCodeLabel);
            this.clientGroup.Controls.Add(this.label6);
            this.clientGroup.Controls.Add(this.contactQtyLabel);
            this.clientGroup.Controls.Add(this.findContactBtn);
            this.clientGroup.Controls.Add(this.findClientBtn);
            this.clientGroup.Controls.Add(this.label4);
            this.clientGroup.Controls.Add(this.contactTextBox);
            this.clientGroup.Controls.Add(this.label3);
            this.clientGroup.Controls.Add(this.clientTextBox);
            this.clientGroup.Controls.Add(this.label2);
            this.clientGroup.Controls.Add(this.clientDiscountLabel);
            this.clientGroup.Controls.Add(this.discountDownLabel);
            this.clientGroup.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.clientGroup.Location = new System.Drawing.Point(24, 23);
            this.clientGroup.Margin = new System.Windows.Forms.Padding(0);
            this.clientGroup.Name = "clientGroup";
            this.clientGroup.Size = new System.Drawing.Size(791, 160);
            this.clientGroup.TabIndex = 0;
            this.clientGroup.TabStop = false;
            this.clientGroup.Text = "Clientes & Contactos";
            // 
            // contactCodeLabel
            // 
            this.contactCodeLabel.AutoSize = true;
            this.contactCodeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.contactCodeLabel.Location = new System.Drawing.Point(486, 123);
            this.contactCodeLabel.Name = "contactCodeLabel";
            this.contactCodeLabel.Size = new System.Drawing.Size(15, 15);
            this.contactCodeLabel.TabIndex = 16;
            this.contactCodeLabel.Text = "0";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(428, 123);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(53, 13);
            this.label13.TabIndex = 15;
            this.label13.Text = "Contacto:";
            // 
            // clientPhoneLabel
            // 
            this.clientPhoneLabel.AutoSize = true;
            this.clientPhoneLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clientPhoneLabel.Location = new System.Drawing.Point(486, 99);
            this.clientPhoneLabel.Name = "clientPhoneLabel";
            this.clientPhoneLabel.Size = new System.Drawing.Size(15, 15);
            this.clientPhoneLabel.TabIndex = 14;
            this.clientPhoneLabel.Text = "0";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(428, 99);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(52, 13);
            this.label8.TabIndex = 13;
            this.label8.Text = "Teléfono:";
            // 
            // clientCodeLabel
            // 
            this.clientCodeLabel.AutoSize = true;
            this.clientCodeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clientCodeLabel.Location = new System.Drawing.Point(486, 73);
            this.clientCodeLabel.Name = "clientCodeLabel";
            this.clientCodeLabel.Size = new System.Drawing.Size(15, 15);
            this.clientCodeLabel.TabIndex = 12;
            this.clientCodeLabel.Text = "0";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(437, 73);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(43, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "Código:";
            // 
            // contactQtyLabel
            // 
            this.contactQtyLabel.AutoSize = true;
            this.contactQtyLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.contactQtyLabel.Location = new System.Drawing.Point(486, 47);
            this.contactQtyLabel.Name = "contactQtyLabel";
            this.contactQtyLabel.Size = new System.Drawing.Size(15, 15);
            this.contactQtyLabel.TabIndex = 10;
            this.contactQtyLabel.Text = "0";
            // 
            // findContactBtn
            // 
            this.findContactBtn.BackgroundImage = global::Floberman.Properties.Resources.search;
            this.findContactBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.findContactBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.findContactBtn.Location = new System.Drawing.Point(283, 102);
            this.findContactBtn.Margin = new System.Windows.Forms.Padding(10);
            this.findContactBtn.Name = "findContactBtn";
            this.findContactBtn.Padding = new System.Windows.Forms.Padding(10);
            this.findContactBtn.Size = new System.Drawing.Size(20, 20);
            this.findContactBtn.TabIndex = 9;
            this.findContactBtn.UseVisualStyleBackColor = true;
            this.findContactBtn.Click += new System.EventHandler(this.findContactBtn_Click);
            // 
            // findClientBtn
            // 
            this.findClientBtn.BackgroundImage = global::Floberman.Properties.Resources.search;
            this.findClientBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.findClientBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.findClientBtn.Location = new System.Drawing.Point(283, 46);
            this.findClientBtn.Margin = new System.Windows.Forms.Padding(10);
            this.findClientBtn.Name = "findClientBtn";
            this.findClientBtn.Padding = new System.Windows.Forms.Padding(10);
            this.findClientBtn.Size = new System.Drawing.Size(20, 20);
            this.findClientBtn.TabIndex = 8;
            this.findClientBtn.UseVisualStyleBackColor = true;
            this.findClientBtn.Click += new System.EventHandler(this.findClientBtn_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(394, 49);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(86, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Cant. Contactos:";
            // 
            // contactTextBox
            // 
            this.contactTextBox.Cursor = System.Windows.Forms.Cursors.Default;
            this.contactTextBox.Location = new System.Drawing.Point(29, 103);
            this.contactTextBox.Name = "contactTextBox";
            this.contactTextBox.Size = new System.Drawing.Size(250, 20);
            this.contactTextBox.TabIndex = 1;
            this.contactTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.contactTextBox_KeyDown);
            this.contactTextBox.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.contactTextBox_MouseDoubleClick);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(28, 87);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(50, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Contacto";
            // 
            // clientTextBox
            // 
            this.clientTextBox.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.clientTextBox.Location = new System.Drawing.Point(27, 46);
            this.clientTextBox.Name = "clientTextBox";
            this.clientTextBox.Size = new System.Drawing.Size(250, 20);
            this.clientTextBox.TabIndex = 0;
            this.clientTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.clientTextBox_KeyDown);
            this.clientTextBox.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.clientTextBox_MouseDoubleClick);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(26, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Cliente";
            // 
            // clientDiscountLabel
            // 
            this.clientDiscountLabel.AutoSize = true;
            this.clientDiscountLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clientDiscountLabel.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.clientDiscountLabel.Location = new System.Drawing.Point(682, 54);
            this.clientDiscountLabel.Name = "clientDiscountLabel";
            this.clientDiscountLabel.Size = new System.Drawing.Size(67, 39);
            this.clientDiscountLabel.TabIndex = 1;
            this.clientDiscountLabel.Text = "0%";
            // 
            // discountDownLabel
            // 
            this.discountDownLabel.AutoSize = true;
            this.discountDownLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.discountDownLabel.Location = new System.Drawing.Point(686, 93);
            this.discountDownLabel.Name = "discountDownLabel";
            this.discountDownLabel.Size = new System.Drawing.Size(83, 13);
            this.discountDownLabel.TabIndex = 0;
            this.discountDownLabel.Text = "DESCUENTO";
            // 
            // addOffertBtn
            // 
            this.addOffertBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.addOffertBtn.Location = new System.Drawing.Point(381, 32);
            this.addOffertBtn.Name = "addOffertBtn";
            this.addOffertBtn.Size = new System.Drawing.Size(111, 23);
            this.addOffertBtn.TabIndex = 5;
            this.addOffertBtn.Text = "Agregar Oferta";
            this.addOffertBtn.UseVisualStyleBackColor = true;
            this.addOffertBtn.Click += new System.EventHandler(this.addOffertBtn_Click);
            // 
            // offerGridView
            // 
            this.offerGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.offerGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.offerGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.offerGridView.Location = new System.Drawing.Point(24, 211);
            this.offerGridView.Name = "offerGridView";
            this.offerGridView.RowHeadersVisible = false;
            this.offerGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.offerGridView.Size = new System.Drawing.Size(792, 222);
            this.offerGridView.TabIndex = 1;
            this.offerGridView.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.offerGridView_CellContentDoubleClick);
            this.offerGridView.KeyDown += new System.Windows.Forms.KeyEventHandler(this.offerGridView_KeyDown);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(25, 195);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(64, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "OFERTAS";
            // 
            // addOfferContainer
            // 
            this.addOfferContainer.BackColor = System.Drawing.Color.White;
            this.addOfferContainer.Controls.Add(this.label15);
            this.addOfferContainer.Controls.Add(this.label14);
            this.addOfferContainer.Controls.Add(this.label11);
            this.addOfferContainer.Controls.Add(this.priceTextBox);
            this.addOfferContainer.Controls.Add(this.discountAmountLabel);
            this.addOfferContainer.Controls.Add(this.label12);
            this.addOfferContainer.Controls.Add(this.discountPriceLabel);
            this.addOfferContainer.Controls.Add(this.priceLabel);
            this.addOfferContainer.Controls.Add(this.label10);
            this.addOfferContainer.Controls.Add(this.label9);
            this.addOfferContainer.Controls.Add(this.productNameLabel);
            this.addOfferContainer.Controls.Add(this.findProductBtn);
            this.addOfferContainer.Controls.Add(this.label7);
            this.addOfferContainer.Controls.Add(this.label1);
            this.addOfferContainer.Controls.Add(this.discountTextBox);
            this.addOfferContainer.Controls.Add(this.productTextBox);
            this.addOfferContainer.Controls.Add(this.addOffertBtn);
            this.addOfferContainer.Location = new System.Drawing.Point(24, 442);
            this.addOfferContainer.Name = "addOfferContainer";
            this.addOfferContainer.Size = new System.Drawing.Size(792, 90);
            this.addOfferContainer.TabIndex = 10;
            this.addOfferContainer.TabStop = false;
            this.addOfferContainer.Text = " Agregar Oferta";
            // 
            // discountAmountLabel
            // 
            this.discountAmountLabel.AutoSize = true;
            this.discountAmountLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.discountAmountLabel.Location = new System.Drawing.Point(647, 46);
            this.discountAmountLabel.Name = "discountAmountLabel";
            this.discountAmountLabel.Size = new System.Drawing.Size(14, 13);
            this.discountAmountLabel.TabIndex = 17;
            this.discountAmountLabel.Text = "0";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(580, 46);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(62, 13);
            this.label12.TabIndex = 16;
            this.label12.Text = "Descuento:";
            // 
            // discountPriceLabel
            // 
            this.discountPriceLabel.AutoSize = true;
            this.discountPriceLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.discountPriceLabel.ForeColor = System.Drawing.SystemColors.Highlight;
            this.discountPriceLabel.Location = new System.Drawing.Point(647, 63);
            this.discountPriceLabel.Name = "discountPriceLabel";
            this.discountPriceLabel.Size = new System.Drawing.Size(16, 16);
            this.discountPriceLabel.TabIndex = 15;
            this.discountPriceLabel.Text = "0";
            // 
            // priceLabel
            // 
            this.priceLabel.AutoSize = true;
            this.priceLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.priceLabel.Location = new System.Drawing.Point(647, 30);
            this.priceLabel.Name = "priceLabel";
            this.priceLabel.Size = new System.Drawing.Size(14, 13);
            this.priceLabel.TabIndex = 14;
            this.priceLabel.Text = "0";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(528, 65);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(116, 13);
            this.label10.TabIndex = 13;
            this.label10.Text = "Precio con Descuento:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(601, 30);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(40, 13);
            this.label9.TabIndex = 12;
            this.label9.Text = "Precio:";
            // 
            // productNameLabel
            // 
            this.productNameLabel.AutoSize = true;
            this.productNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.productNameLabel.Location = new System.Drawing.Point(31, 65);
            this.productNameLabel.Name = "productNameLabel";
            this.productNameLabel.Size = new System.Drawing.Size(0, 16);
            this.productNameLabel.TabIndex = 11;
            // 
            // findProductBtn
            // 
            this.findProductBtn.BackgroundImage = global::Floberman.Properties.Resources.search;
            this.findProductBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.findProductBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.findProductBtn.Location = new System.Drawing.Point(135, 33);
            this.findProductBtn.Margin = new System.Windows.Forms.Padding(10);
            this.findProductBtn.Name = "findProductBtn";
            this.findProductBtn.Padding = new System.Windows.Forms.Padding(10);
            this.findProductBtn.Size = new System.Drawing.Size(20, 20);
            this.findProductBtn.TabIndex = 10;
            this.findProductBtn.UseVisualStyleBackColor = true;
            this.findProductBtn.Click += new System.EventHandler(this.findProductBtn_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(176, 17);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(59, 13);
            this.label7.TabIndex = 9;
            this.label7.Text = "Descuento";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(31, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Producto";
            // 
            // discountTextBox
            // 
            this.discountTextBox.Location = new System.Drawing.Point(179, 33);
            this.discountTextBox.Name = "discountTextBox";
            this.discountTextBox.Size = new System.Drawing.Size(76, 20);
            this.discountTextBox.TabIndex = 3;
            this.discountTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.discountTextBox_KeyDown);
            // 
            // productTextBox
            // 
            this.productTextBox.Location = new System.Drawing.Point(31, 33);
            this.productTextBox.Name = "productTextBox";
            this.productTextBox.Size = new System.Drawing.Size(100, 20);
            this.productTextBox.TabIndex = 2;
            this.productTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.productTextBox_KeyDown);
            this.productTextBox.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.productTextBox_MouseDoubleClick);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(280, 18);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(37, 13);
            this.label11.TabIndex = 19;
            this.label11.Text = "Precio";
            // 
            // priceTextBox
            // 
            this.priceTextBox.Location = new System.Drawing.Point(283, 34);
            this.priceTextBox.Name = "priceTextBox";
            this.priceTextBox.Size = new System.Drawing.Size(76, 20);
            this.priceTextBox.TabIndex = 4;
            this.priceTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.priceTextBox_KeyDown);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(367, 39);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(0, 13);
            this.label14.TabIndex = 20;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(261, 37);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(13, 13);
            this.label15.TabIndex = 21;
            this.label15.Text = "ó";
            // 
            // Home
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(248)))), ((int)(((byte)(250)))));
            this.ClientSize = new System.Drawing.Size(844, 544);
            this.Controls.Add(this.addOfferContainer);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.offerGridView);
            this.Controls.Add(this.clientGroup);
            this.Name = "Home";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Mantenimiento de Ofertas";
            this.clientGroup.ResumeLayout(false);
            this.clientGroup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.offerGridView)).EndInit();
            this.addOfferContainer.ResumeLayout(false);
            this.addOfferContainer.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox clientGroup;
        private System.Windows.Forms.Label discountDownLabel;
        private System.Windows.Forms.DataGridView offerGridView;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.Button addOffertBtn;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox contactTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox clientTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label clientDiscountLabel;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button findClientBtn;
        private System.Windows.Forms.Button findContactBtn;
        private System.Windows.Forms.Label clientCodeLabel;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label contactQtyLabel;
        private System.Windows.Forms.Label clientPhoneLabel;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox addOfferContainer;
        private System.Windows.Forms.Button findProductBtn;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox discountTextBox;
        private System.Windows.Forms.TextBox productTextBox;
        private System.Windows.Forms.Label productNameLabel;
        private System.Windows.Forms.Label priceLabel;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label discountPriceLabel;
        private System.Windows.Forms.Label discountAmountLabel;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label contactCodeLabel;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox priceTextBox;
    }
}

