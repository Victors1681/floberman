﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Floberman.Controllers
{
    public class LoginController
    {

        public bool startSession(string user, string password)
        {
            bool loginOk = false;

            DataClassesDataContext db = new DataClassesDataContext();

            var userdata = (from u in db.UsersViews
                            where
u.USUARIO.Trim().Equals(user.Trim()) &&
u.CLAVE.Trim().Equals(password.Trim())
                            select u).FirstOrDefault();

            if(userdata != null)
            {
                Properties.Settings.Default.userName = userdata.NOMBRE;
                Properties.Settings.Default.userCode = userdata.COD_USUARIO;
                loginOk = true;
            }



            return loginOk;

        }
    }
}
