﻿using Floberman.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Floberman.Controllers
{
   public class InventoryController
    {

        public void getProductByName(string name, DataGridView dataGridView)
        {
            DataClassesDataContext db = new DataClassesDataContext();


            var dataset = (from product in db.ProductViews
                           where product.DESCR.Contains(name)
                           orderby product.NUM_REG
                           select new
                           {
                               product.CLV_ART,
                               DESCR = (product.DESCR == null) ? "" : product.DESCR,
                               product.EXIST,
                               product.precio1itbis,
                               product.precio2itbis,
                               product.precio3itbis,
                               product.precio4itbis,
                               product.precio5itbis,
                               product.PRECIO1,
                               product.PRECIO2,
                               product.PRECIO3,
                               product.PRECIO4,
                               product.PRECIO5,

                           }).Take(40);

            dataGridView.DataSource = dataset;
            dataGridView.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dataGridView.Columns[8].Visible = false;
            dataGridView.Columns[9].Visible = false;
            dataGridView.Columns[10].Visible = false;
            dataGridView.Columns[11].Visible = false;
            dataGridView.Columns[12].Visible = false;
        }


        public Product getProductById(string id)
        {
            Product product = new Product();
            DataClassesDataContext db = new DataClassesDataContext();

            var result = (from inve in db.ProductViews
                          where inve.CLV_ART.Trim().Equals(id)
                          select inve).First();
            
            if(result != null)
            { 

                product.name = result.DESCR.Trim();
                product.productId = result.CLV_ART.Trim();
                product.stock = result.EXIST ?? 0;
                product.price1 = result.PRECIO1 ?? 0;
                product.price2 = result.PRECIO2 ?? 0;
                product.price3 = result.PRECIO3 ?? 0;
                product.price4 = result.PRECIO4 ?? 0;
                product.price5 = result.PRECIO5 ?? 0;

                product.price1Itbis = result.precio1itbis ?? 0;
                product.price2Itbis = result.precio2itbis ?? 0;
                product.price3Itbis = result.precio3itbis ?? 0;
                product.price4Itbis = result.precio4itbis ?? 0;
                product.price5Itbis = result.precio5itbis ?? 0;

                return product;
            }

            return null;

        }
    }
}
