﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Floberman.Controllers
{
    public class ConnectionDB
    {

        public bool iniConfiguration()
        {
            bool isUdlOk = false;
            try
            {

                string text = System.IO.File.ReadAllText(@"C:\ofertas.udl");
                var arr = text.Split(';');

                var dataSource = "";

                foreach (var r in arr)
                {
                    if (r.Contains("Data Source="))
                    {
                        string ds = r.Replace("Data Source=", "");
                        dataSource = ds;
                    }
                }

                //string connection = $"Data Source={dataSource};Initial Catalog=MBSPedido;Integrated Security=True";
                string connection = $"Data Source={dataSource};Initial Catalog=MBSPedido;Persist Security Info=True;User ID=iphone;Password=Solu@74x1";


                Properties.Settings.Default.MBSPedidoConnectionStringUser = connection;
                Properties.Settings.Default.Save();
                isUdlOk = true;
            }
            catch(Exception e)
            {
                MessageBox.Show($"Verifique c:\\ofertas.udl", "Error de conexión", MessageBoxButtons.OK, MessageBoxIcon.Error);
                isUdlOk = false;
            }

            return isUdlOk;
        }

    }
}
