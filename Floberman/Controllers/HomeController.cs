﻿using System;
using System.Collections.Generic;
using System.Linq;
using Floberman.Model;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Floberman.Controllers
{
    public class HomeController
    {

        public Tuple<double, double> getDiscountPrice(string discountText, Client client, Product product, double inPrice)
        {

            float granDiscount = float.Parse(discountText) + client.discount;

            float discountPercentage = granDiscount / 100;

            double price = inPrice > 0 ? inPrice : product.price1;
            double itbis = product.price1Itbis - price;

            double discountPrice = price * discountPercentage;

            double lastPrice = (price - discountPrice) + itbis;

            return Tuple.Create(lastPrice, discountPrice);
        }

        public bool removeOffer(DataGridView gridVeiw)
        {
            bool removed = false;

            var cells = gridVeiw.CurrentRow.Cells;

            DataClassesDataContext db = new DataClassesDataContext();

            string client = cells["clientId"].Value.ToString();
            int contact = int.Parse(cells["contactId"].Value.ToString());
            string product = cells["productId"].Value.ToString();

            var deleteOffer = from offer in db.Offers
                              where
      offer.clientId.Trim().Equals(client.Trim()) && offer.contactId.Equals(contact) && offer.productId.Trim().Equals(product.Trim())
                              select offer;

            foreach (var offer in deleteOffer)
            {
                db.Offers.DeleteOnSubmit(offer);
            }

            try
            {
                db.SubmitChanges();
                removed = true;
            }catch (Exception e)
            {
                Console.WriteLine(e);
                removed = false;
            }

            return removed;
            


        }


        public IEnumerable<object> getOffers(string clientId, int contactId)
        {
            DataClassesDataContext db = new DataClassesDataContext();

            var offers = from offer in db.Offers
                         join client in db.ClientViews on offer.clientId.Trim() equals client.CCLIE.Trim()
                         join product in db.ProductViews on offer.productId.Trim() equals product.CLV_ART.Trim()
                         join contact in db.ContactViews on offer.contactId equals contact.NUM_REG
                         where offer.clientId.Trim() == clientId.Trim() && offer.contactId == contactId
                         select new
                         {
                             Id = offer.offerId,
                             offer.clientId,
                             offer.contactId,
                             offer.productId,
                             offer.discount,
                             offer.price,
                             Cliente = (client.NOMBRE == null) ? "": client.NOMBRE.Trim(),
                             Contacto = (contact.NOMBRE ==null)? "": contact.NOMBRE.Trim(),
                             Codigo = (product.CLV_ART == null)? "": product.CLV_ART.Trim(),
                             Producto = (product.DESCR == null)? "" : product.DESCR.Trim(),
                             Descuento = offer.discount,
                             Precio = offer.price
                         
                         };

            return offers;
        } 

        private bool isOfferExist(Offer offer)
        {
            
            DataClassesDataContext db = new DataClassesDataContext();

            int result = (from o in db.Offers
                          where o.clientId.Trim().Equals(offer.clientId.Trim()) &&
                          o.contactId.Equals(offer.contactId) &&
                          o.productId.Trim().Equals(offer.productId.Trim())
                          select o
                          ).Count();

            return result > 0 ?  true : false;

        }

        public bool isProductExist(string productId)
        {
            DataClassesDataContext db = new DataClassesDataContext();

            int result = (from i in db.ProductViews
                          where i.CLV_ART.Trim().Equals(productId) select i).Count();


            return result > 0 ? true : false;
        }

        public Tuple<bool, string> insertOffer(Offer offer)
        {
            bool isInserted = true;
            string msg = "";

            if (!this.isOfferExist(offer))
            {
               
                DataClassesDataContext db = new DataClassesDataContext();

                db.Offers.InsertOnSubmit(offer);

                try
                {
                    db.SubmitChanges();
                    isInserted = true;
                    msg = "Oferta insertada correctamente";
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    db.SubmitChanges();
                    isInserted = false;
                    msg = $"Error al insertar la offerta {e}";
                }
            }
            else
            {
                isInserted = false;
                msg = "Offerta ya existe!";
            }

            return Tuple.Create(isInserted, msg);

        }

    }
     
}
