﻿using Floberman.Controllers;
using Floberman.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Floberman.Views
{
    public partial class ClientList : Form
    {

        private ViewSelected viewSelected = ViewSelected.Client;
        private Home parentView = null;

        public ClientList(Home parentView, ViewSelected viewSelected)
        {
            this.viewSelected = viewSelected;
            this.parentView = parentView;
            
            InitializeComponent();
            

            switch(viewSelected)
            {
                case ViewSelected.Client:
                    titleLabel.Text = "Nombre del Cliente";
                    break;
                case ViewSelected.Contact:
                    titleLabel.Text = "Nombre del Contacto";
                break;
            }

            this.loadData(viewSelected);

            this.clientTextBox.Select();
        }


        private void loadData(ViewSelected viewSelected)
        {
            

            switch (viewSelected)
            {
                case ViewSelected.Client:
                    dataGridView.DataSource = this.getClientByName("");
                    break;
                case ViewSelected.Contact:
                    dataGridView.DataSource = this.getContactByName("");
                    break;
            }
        }

        private IEnumerable<object> getClientByName(string name)
        {
            DataClassesDataContext db = new DataClassesDataContext();

            var dataset = (from client in db.ClientViews
             where client.NOMBRE.Contains(name) && client.CCLIE.Trim() != ""
             orderby client.CCLIE
             select new { CCLIE = client.CCLIE.Trim(),
                 NOMBRE = (client.NOMBRE == null)? "" : client.NOMBRE.Trim(),
                 DIR = (client.DIR == null)? "" : client.DIR.Trim(),
                 POB = (client.POB == null) ? "" : client.POB.Trim(),
                 TELEFONO = (client.TELEFONO == null)? "" : client.TELEFONO.Trim(),
                 client.DESCUENTO
             }).Take(40);

            return dataset;

        }

        private IEnumerable<object> getContactByName(string name)
        {
            DataClassesDataContext db = new DataClassesDataContext();

            var dataset = (from contact in db.ContactViews
                           where contact.NOMBRE.Contains(name) && contact.CCLIE.Trim().Equals(this.parentView.client.code.Trim())
                           orderby contact.CCLIE
                           select new {
                               CCLIE = contact.CCLIE.Trim(),
                               CODE = contact.NUM_REG,
                               NOMBRE = (contact.NOMBRE == null) ? "" : contact.NOMBRE.Trim(),
                               DIRECCION = (contact.DIRECCION == null) ? "" : contact.DIRECCION.Trim(),
                               TELEFONO = (contact.TELEFONO == null) ? "" : contact.TELEFONO.Trim()
                           }).Take(40);

            return dataset;

        }

        private int getTotalContactbyClientId(string clientId)
        {
            DataClassesDataContext db = new DataClassesDataContext();

            int contacts = (from contact in db.ContactViews where contact.CCLIE.Trim().Equals(clientId.Trim()) select contact).Count();

            return contacts;
        }

        private void clientTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void clientTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                string findText = clientTextBox.Text;

                if (this.viewSelected == ViewSelected.Client)
                {
                    dataGridView.DataSource = this.getClientByName(findText);
                }
                else
                {
                    dataGridView.DataSource = this.getContactByName(findText);
                }

                dataGridView.Focus();
            }else if(e.KeyCode == Keys.Down)
            {
                dataGridView.Select();
            }
        }
 

        private void dataGridView_KeyDown(object sender, KeyEventArgs e)
        {

            if(e.KeyCode == Keys.Enter)
            {
                e.Handled = true;
                this.selectRow();
            }

        }

 

        private void selectRow()
        {
            var cells = dataGridView.CurrentRow.Cells;
            switch (this.viewSelected)
            {
                case ViewSelected.Client:
                    Client client = new Client();

                    client.code = cells["CCLIE"].Value.ToString();
                    client.name = cells["NOMBRE"].Value.ToString();
                    client.address = cells["DIR"].Value.ToString();
                    client.phone = cells["TELEFONO"].Value.ToString();
                    client.discount = float.Parse(cells["DESCUENTO"].Value.ToString());

                    client.contactQty = this.getTotalContactbyClientId(client.code);

                    dataGridView.CurrentRow.Selected = true;
                   
                    this.parentView.client = client;
                    this.parentView.updateClientUI();
                    break;

                case ViewSelected.Contact:
                    Contact contact = new Contact();

                    contact.code = Int32.Parse(cells["CODE"].Value.ToString());
                    contact.client = cells["CCLIE"].Value.ToString();
                    contact.name = cells["NOMBRE"].Value.ToString();
                    contact.address = cells["DIRECCION"].Value.ToString();
                    contact.phone = cells["TELEFONO"].Value.ToString();

                    this.parentView.contact = contact;
                    this.parentView.updateContactUI();

                    break;
            }

            this.Dispose();
        }
        

        private void dataGridView_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            this.selectRow();
        }
    }
}
