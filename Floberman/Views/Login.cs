﻿using Floberman.Controllers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Deployment.Application;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Floberman.Views
{
    public partial class Login : Form
    {
        LoginController loginController = new LoginController();
        Home homeView = null;
        public Login(Home homeView)
        {
            this.homeView = homeView;
            this.homeView.Visible = false;
            InitializeComponent();
            //showVersion();
        }

        private void showVersion()
        {
            string version = string.Format("v{0}", ApplicationDeployment.CurrentDeployment.CurrentVersion.ToString(4));
            this.versionLabel.Text = version;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void addOffertBtn_Click(object sender, EventArgs e)
        {
            startSession();
        }

        private void passwordTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                startSession();
            }
        }

        private void startSession()
        {
            string user = userTextBox.Text;
            string password = passwordTextBox.Text;

            if (loginController.startSession(user, password))
            {
                this.Visible = false;
                this.homeView.Visible = true;
            }
            else
            {
                //access error
            }
        }

        private void cancelBtn_Click(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }
    }

}
