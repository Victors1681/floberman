﻿using Floberman.Controllers;
using Floberman.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Floberman.Views
{
    public partial class Inventory : Form
    {
        private Home parentView = null;
        private InventoryController controller = new InventoryController();

        public Inventory(Home homeView)
        {
            InitializeComponent();
            this.controller.getProductByName("", dataGridView);
            this.parentView = homeView;
        }

        private void Inventory_Load(object sender, EventArgs e)
        {

        }

        private void productTextBox_KeyDown(object sender, KeyEventArgs e)
        {
         
            if(e.KeyCode == Keys.Enter)
            {
                string find = productTextBox.Text;
                this.controller.getProductByName(find, dataGridView);
                dataGridView.Focus();
            }
        }

      
        private void dataGridView_KeyDown(object sender, KeyEventArgs e)
        {

            if(e.KeyCode == Keys.Enter)
            {
                e.Handled = true;
                this.selectRow();
            }
        }

        private void dataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            this.selectRow();
        }

        private void selectRow()
        {
            
            var cells = dataGridView.CurrentRow.Cells;

            //Update inventario model object.

            Product p = new Product();

            p.productId = cells["CLV_ART"].Value.ToString();
            p.name = cells["DESCR"].Value.ToString();
            p.stock = float.Parse(cells["EXIST"].Value.ToString());
            p.price1 = float.Parse(cells["PRECIO1"].Value.ToString());
            p.price2 = float.Parse(cells["PRECIO2"].Value.ToString());
            p.price3 = float.Parse(cells["PRECIO3"].Value.ToString());
            p.price4 = float.Parse(cells["PRECIO4"].Value.ToString());
            p.price5 = float.Parse(cells["PRECIO5"].Value.ToString());

            p.price1Itbis = float.Parse(cells["precio1itbis"].Value.ToString());
            p.price2Itbis = float.Parse(cells["precio2itbis"].Value.ToString());
            p.price3Itbis = float.Parse(cells["precio3itbis"].Value.ToString());
            p.price4Itbis = float.Parse(cells["precio4itbis"].Value.ToString());
            p.price5Itbis = float.Parse(cells["precio5itbis"].Value.ToString());


            this.parentView.product = p;
            this.parentView.updateProductUIfromInventory();

            this.Dispose();
        }
    }
}
