﻿namespace Floberman.Views
{
    partial class UpdateOffer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.discountTextBox = new System.Windows.Forms.TextBox();
            this.editOffertBtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.priceTextBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // discountTextBox
            // 
            this.discountTextBox.Location = new System.Drawing.Point(25, 37);
            this.discountTextBox.Name = "discountTextBox";
            this.discountTextBox.Size = new System.Drawing.Size(141, 20);
            this.discountTextBox.TabIndex = 0;
            this.discountTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.discountTextBox_KeyDown);
            // 
            // editOffertBtn
            // 
            this.editOffertBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.editOffertBtn.Location = new System.Drawing.Point(92, 73);
            this.editOffertBtn.Name = "editOffertBtn";
            this.editOffertBtn.Size = new System.Drawing.Size(173, 23);
            this.editOffertBtn.TabIndex = 8;
            this.editOffertBtn.Text = "Actualizar";
            this.editOffertBtn.UseVisualStyleBackColor = true;
            this.editOffertBtn.Click += new System.EventHandler(this.editOffertBtn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(22, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(144, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Porciento de Descuento";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(200, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Precio";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // priceTextBox
            // 
            this.priceTextBox.Location = new System.Drawing.Point(203, 37);
            this.priceTextBox.Name = "priceTextBox";
            this.priceTextBox.Size = new System.Drawing.Size(136, 20);
            this.priceTextBox.TabIndex = 2;
            this.priceTextBox.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // UpdateOffer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(248)))), ((int)(((byte)(250)))));
            this.ClientSize = new System.Drawing.Size(381, 108);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.priceTextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.editOffertBtn);
            this.Controls.Add(this.discountTextBox);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "UpdateOffer";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Actualizar Descuento";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.UpdateOffer_KeyDown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox discountTextBox;
        private System.Windows.Forms.Button editOffertBtn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox priceTextBox;
    }
}