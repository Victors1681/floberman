﻿using Floberman.Controllers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Floberman.Views
{
    public partial class UpdateOffer : Form
    {

        private Home parentView = null;
        private string productId { get; set; }
        private int rowIndex { get; set; }

        public UpdateOffer(double discount, double price, Home parentView, string productId, int rowIndex)
        {
            this.parentView = parentView;
            this.productId = productId;
            this.rowIndex = rowIndex;

            InitializeComponent();

            this.discountTextBox.Text = discount.ToString("n2");
            this.priceTextBox.Text = price.ToString("n2");
        }

        private void editOffertBtn_Click(object sender, EventArgs e)
        {
            this.updateDiscount();
        }

        private void updateDiscount()
        {
            double discount;
            float price;
            var Result = double.TryParse(discountTextBox.Text, out discount);
            var priceResult = float.TryParse(priceTextBox.Text, out price);
            if (Result)
            {
                if (discount > 0 && discount < 99.0)
                {

                    DataClassesDataContext db = new DataClassesDataContext();

                    var query = from offer in db.Offers
                                where
        offer.clientId.Trim().Equals(this.parentView.client.code) &&
        offer.contactId.Equals(this.parentView.contact.code) &&
        offer.productId.Trim().Equals(this.productId)
                                select offer;

                    foreach (Offer o in query)
                    {
                        o.discount = discount;
                        o.price = price;
                        o.userId = Properties.Settings.Default.userCode;
                    }

                    try
                    {
                        db.SubmitChanges();
                        this.parentView.reloadGridView(this.rowIndex);
                        this.Dispose();

                    }
                    catch (Exception err)
                    {
                        Console.WriteLine(err);
                    }

                }
                else
                {
                    MessageBox.Show("Descuento Inválido!");
                }
            }
        }

        private void discountTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                this.updateDiscount();
            }else if(e.KeyCode == Keys.Escape)
            {
                this.Dispose();
            }
        }

        private void UpdateOffer_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Escape)
            {
                this.Dispose();
            }
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
