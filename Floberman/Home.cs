﻿using Floberman.Controllers;
using Floberman.Model;
using Floberman.Views;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Floberman
{
    public enum ViewSelected { Client, Contact };
    
    public partial class Home : Form
    {
        public Client client = new Client() ;
        public Contact contact = new Contact();
        public bool requireContact = false;
        public Product product = new Product();
        private HomeController controller = new HomeController();
        private InventoryController inventoryController = new InventoryController();


        private bool isProductExist = false;
        private bool isDiscountOk = false;

        public Home()
        {
            ConnectionDB c = new ConnectionDB();

            if (c.iniConfiguration())
            {
                
                InitializeComponent();
                Login login = new Login(this);
                login.ShowDialog();

                this.isContactEnabled(false);

                this.reloadGridView();
            }
        }

        public void reloadGridView(int indexFocus = 0)
        {
            string client = this.client.code == null ? "" : this.client.code; 

            offerGridView.DataSource = this.controller.getOffers(client, this.contact.code);

            offerGridView.Columns["clientId"].Visible = false;
            offerGridView.Columns["contactId"].Visible = false;
            offerGridView.Columns["productId"].Visible = false;
            offerGridView.Columns["discount"].Visible = false;
            offerGridView.Columns["price"].Visible = false;

            if (offerGridView.Rows.Count > 0 && indexFocus <= offerGridView.Rows.Count)
            {
                offerGridView.ClearSelection();
                offerGridView.Rows[indexFocus].Selected = true;
            }


        }

        private void isContactEnabled(bool enable)
        {
            this.contactTextBox.Enabled = enable;
            this.findContactBtn.Enabled = enable;
            this.contactTextBox.Text = "";
            
        }

     
        private void findClientBtn_Click(object sender, EventArgs e)
        {
            ClientList clientForm = new ClientList(this, ViewSelected.Client);
            clientForm.ShowDialog(this);
        }

        private void findContactBtn_Click(object sender, EventArgs e)
        {
            ClientList contactForm = new ClientList(this, ViewSelected.Contact); 
            contactForm.ShowDialog(this);
        }

        public void updateClientUI()
        {
            clientDiscountLabel.Text = $"{this.client.discount}%";
            clientCodeLabel.Text = this.client.code;
            clientPhoneLabel.Text = this.client.phone;
            clientTextBox.Text = this.client.name;
            contactQtyLabel.Text = this.client.contactQty.ToString();


            clientTextBox.ReadOnly = true;

            if (this.client.contactQty > 0)
            {
                this.isContactEnabled(true);
                this.requireContact = true;
            }
            else
            {
                this.isContactEnabled(false);
                requireContact = false;
                this.reloadGridView();
            }
        }

        public void updateContactUI()
        {
            contactTextBox.Text = this.contact.name.Trim();
            contactCodeLabel.Text = this.contact.code.ToString();
            this.reloadGridView();
        }

        private void clientTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (this.clientTextBox.Text == "")
            {
                this.findClientBtn_Click(sender, e);
            }else if(this.clientTextBox.Text != "" && this.client.contactQty > 0)
            {
                this.contactTextBox.Select();
            }else if(this.clientTextBox.Text != "" && this.client.contactQty == 0)
            {
                this.productTextBox.Select();
            }
        }

        private void clientTextBox_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.clientTextBox_KeyDown(sender, null);
        }

        private void contactTextBox_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.contactTextBox_KeyDown(sender, null);
        }

        private void contactTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (contactTextBox.Text == "")
            {
                this.findContactBtn_Click(sender, e);
            }
            else
            {
                this.productTextBox.Select();
            }
        }

        private void findProductBtn_Click(object sender, EventArgs e)
        {
            Inventory inventory = new Inventory(this);
            inventory.ShowDialog();
        }

        private void productTextBox_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.findProductBtn_Click(sender, e);
        }

        private void addOffertBtn_Click(object sender, EventArgs e)
        {

            if (this.validateUI())
            {
                addOffertBtn.Enabled = false;

                string product = productTextBox.Text;
                float discount = float.Parse(discountTextBox.Text);
                float price = float.Parse(priceTextBox.Text);


                var offer = new Offer
                {
                    clientId = this.client.code,
                    contactId = this.contact.code,
                    productId = product.Trim(),
                    discount = discount,
                    modified = DateTime.Now,
                    userId = Properties.Settings.Default.userCode,
                    price = price
                };

                var insert = this.controller.insertOffer(offer);

               if (insert.Item1)
                {

                    this.reset();
                    this.reloadGridView();
                    addOffertBtn.Enabled = true;
                    this.productTextBox.Select();
                }
                else
                {
                    MessageBox.Show(insert.Item2);
                    addOffertBtn.Enabled = true;
                }


            }
        }

        public void updateProductUIfromInventory()
        {
            //Force the user to press enter on the producttextbox
            this.isProductExist = false;
            productTextBox.Text = this.product.productId;

        }

        public void updateProductUI()
        {
            productTextBox.Text = this.product.productId;
            productNameLabel.Text = this.product.name;
            priceLabel.Text = this.product.price1Itbis.ToString();

            discountTextBox.Select();
        }

        private void productTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                if (productTextBox.Text == "")
                {
                    this.findProductBtn_Click(sender, e);
                }
                else
                {
                    if (this.controller.isProductExist(productTextBox.Text))
                    {
                        //update product object

                       this.product = this.inventoryController.getProductById(productTextBox.Text);
                        this.updateProductUI();

                        this.isProductExist = true;
                        this.discountTextBox.Select();
                    }
                    else
                    {
                        this.reset();
                    }
                }

            }
           
        }

        private void discountTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter && discountTextBox.Text != "")
            {
                decimal myDec;
                var Result = decimal.TryParse(discountTextBox.Text, out myDec);
                double price;
                var ResultPrice = double.TryParse(priceTextBox.Text, out price);
                if (Result)
                {
                    if (float.Parse(discountTextBox.Text) > -1 && float.Parse(discountTextBox.Text) < 99.0)
                    {
                        //Calculate discout for price. taking care the client discount
                        var data = this.controller.getDiscountPrice(discountTextBox.Text, this.client, this.product, price);
                        this.discountPriceLabel.Text = data.Item1.ToString("n2");
                        this.discountAmountLabel.Text = data.Item2.ToString("n2");

                        this.priceTextBox.Focus();
                        this.isDiscountOk = true;
                    }
                    else
                    {
                        this.isDiscountOk = false;
                        MessageBox.Show("Verifique el descuento");
                    }

                }
                else
                {
                    discountTextBox.Text = "";
                }
            }
            else
            {
                discountTextBox.Text = "0.0";
            }
        }

        private bool validateUI()
        {
            bool isOk = true;

            double price;
            var priceResult = double.TryParse(priceTextBox.Text, out price);


            if (clientTextBox.Text == "")
            {
                isOk = false;
                clientTextBox.Select();
            }else if(contactTextBox.Text == "" && this.client.contactQty > 0)
            {
                isOk = false;
                contactTextBox.Select();
            }else if(productTextBox.Text == "" || !this.isProductExist)
            {
                isOk = false;
                this.reset();
                productTextBox.Select();
            }else if(discountTextBox.Text == "" || !isDiscountOk)
            {
                isOk = false;
                discountTextBox.Select();
            }
            else if ((discountTextBox.Text == "" || discountTextBox.Text == "0") && price < 1.0)
            {
                isOk = false;
                priceTextBox.Select();
            }


            return isOk;
        }

        private void reset()
        {
            this.product = null; 

            productTextBox.Text = "";
            discountTextBox.Text = "";

            discountPriceLabel.Text = "";
            priceLabel.Text = "";
            discountAmountLabel.Text = "";
            productNameLabel.Text = "";
            priceTextBox.Text = "";

            this.isProductExist = false;
            this.isDiscountOk = false;


        }

        private void resetHeader()
        { 
            this.product = null;

            clientTextBox.Text = "";
            clientCodeLabel.Text = "";
            clientPhoneLabel.Text = "";
            clientDiscountLabel.Text = "";

            contactQtyLabel.Text = "";
            contactCodeLabel.Text = "";
            contactTextBox.Text = "";
        }

        private void offerGridView_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Delete)
            {
                this.removeOffer();
            }else if(e.KeyCode == Keys.Enter)
            {
                e.Handled = true;
                this.editOffer();

            }
        }

        private void removeOffer()
        {
            DialogResult confirmation = MessageBox.Show("Seguro que desea eliminar esta oferta?", "Warning", MessageBoxButtons.YesNo);
            if (confirmation == DialogResult.Yes)
            {
                bool removed = this.controller.removeOffer(offerGridView);

                if (removed)
                {
                    this.reloadGridView();
                }
            }
        }

   
        private void offerGridView_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void editOffer()
        {
            var cells = offerGridView.CurrentRow.Cells;
            var index = offerGridView.CurrentRow.Index;

            string productId = cells["productId"].Value.ToString();
            double discount = double.Parse(cells["discount"].Value.ToString());
            double price = double.Parse(cells["price"].Value.ToString());

            UpdateOffer updateOffer = new UpdateOffer(discount, price, this, productId, index);
            updateOffer.ShowDialog();
        }

        private void priceTextBox_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Enter && priceTextBox.Text != "")
            {
                double price;
                var Result = double.TryParse(discountTextBox.Text, out price);
                if (Result)
                {
                    if (float.Parse(priceTextBox.Text) > 0 && float.Parse(discountTextBox.Text) < 99.0)
                    {
                        //Calculate discout for price. taking care the client discount
                        var data = this.controller.getDiscountPrice(discountTextBox.Text, this.client, this.product, price);
                        this.discountPriceLabel.Text = data.Item1.ToString("n2");
                        this.discountAmountLabel.Text = data.Item2.ToString("n2");

                        this.addOffertBtn.Focus();
                        this.isDiscountOk = true;
                    }
                    else
                    {
                        this.isDiscountOk = false;
                        MessageBox.Show("Verifique el descuento");
                    }

                }
                else
                {
                    discountTextBox.Text = "";
                }
            }

        }
    }
}
